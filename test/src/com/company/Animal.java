package com.company;

public class Animal {


    boolean vegetarian;
    String eats;
    int noOfLegs;


    public boolean isVegetarian() {
        return vegetarian;
    }

    public String getEats() {
        return eats;
    }

    public int getNoOfLegs() {
        return noOfLegs;
    }

    public Animal(){

    }

    public Animal(boolean vegetarian, String eats, int noOfLegs) {
        this.vegetarian = vegetarian;
        this.eats = eats;
        this.noOfLegs = noOfLegs;
    }


    @Override
    public String toString() {
        return " vegetarian = " + vegetarian
                + ", eats = " + eats
                + ", noOfLegs = " + noOfLegs;

    }
}

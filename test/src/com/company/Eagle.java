package com.company;

public class Eagle extends Bird {
    @Override
    public String toString() {
        return super.toString ();
    }

    int crulya;

    public int getCrulya() {
        return crulya;
    }

    public void setCrulya(int crulya) {

        this.crulya = crulya;
    }

    public Eagle(int crulya) {
        this.crulya = crulya;
    }

    public Eagle(boolean vegetarian, String eats, int noOfLegs, int crulya) {
        super ( vegetarian, eats, noOfLegs );
        this.crulya = crulya;
    }
    public Eagle(boolean vegetarian, int noOfLegs, int crulya) {
        super (vegetarian, "heal", noOfLegs);
        this.crulya = crulya;
    }
}

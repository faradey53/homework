package com.company;

public class Shark extends Fish {

    @Override
    public String toString() {
        return super.toString () + ", zub = "+ zub;
    }

    int zub;

    public int getZub() {
        return zub;
    }

    public void setZub(int zub) {
        this.zub = zub;
    }

    public Shark(int zub) {
        this.zub = zub;
    }

    public Shark(boolean vegetarian, String eats, int noOfLegs, int zub) {
        super ( vegetarian, eats, noOfLegs );
        this.zub = zub;
    }
    public Shark(boolean vegetarian, int noOfLegs, int zub) {
        super ( vegetarian, "meat", noOfLegs );
        this.zub = zub;
    }
}

package com.company;

public class Сow extends Mammal {
    @Override
    public String toString() {
        return super.toString ();
    }

    public Сow(int hoof) {
        this.hoof = hoof;
    }

    public int getHoof() {
        return hoof;
    }

    public void setHoof(int hoof) {
        this.hoof = hoof;
    }

    public Сow(boolean vegetarian, String eats, int noOfLegs, int hoof) {
        super ( vegetarian, eats, noOfLegs );
        this.hoof = hoof;
    }

    public Сow(boolean vegetarian, int noOfLegs, int hoof) {
        super ( vegetarian, "juki", noOfLegs );
        this.hoof = hoof;
    }


    private int hoof;
}
